package gitlab

import (
	"encoding/json"
	"fmt"
)

// ProjectID indicates the project's identifier
type ProjectID uint64

// Project represents a GitLab project
type Project struct {
	client *Client

	Archived      bool      `json:"archived"`
	AvatarURL     *string   `json:"avatar_url"`
	CiConfigPath  string    `json:"ci_config_path"`
	CreatedAt     string    `json:"created_at"`
	DefaultBranch string    `json:"default_branch"`
	Description   string    `json:"description"`
	ForksCount    uint64    `json:"forks_cout"`
	HTTPURLToRepo *string   `json:"http_url_to_repo"`
	ID            ProjectID `json:"id"`
	Links         struct {
		Self          *string `json:"self"`
		Events        *string `json:"events"`
		Labels        *string `json:"labels"`
		Members       *string `json:"members"`
		MergeRequests *string `json:"merge_requests"`
		RepoBranches  *string `json:"repo_branches"`
	} `json:"_links"`
	LastActivityAt    string  `json:"last_activity_at"`
	Name              string  `json:"name"`
	NameWithNamespace string  `json:"name_with_namespace"`
	Owner             User    `json:"owner"`
	Path              string  `json:"path"`
	PathWithNamespace string  `json:"path_with_namespace"`
	ReadmeURL         *string `json:"readme_url"`
	SSHURLToRepo      *string `json:"ssh_url_to_repo"`
	StarCount         uint64  `json:"star_count"`
	Visibility        string  `json:"visibility"`
	WebURL            string  `json:"web_url"`
}

// MergeRequests returns a list of merge requests
func (p *Project) MergeRequests(attributes *Attributes) ([]*MergeRequest, error) {
	var endpoint string
	if p.Links.MergeRequests != nil {
		endpoint = *p.Links.MergeRequests
	} else {
		endpoint = fmt.Sprintf("%s/projects/%d/merge_requests", p.client.url, p.ID)
	}

	resp, err := p.client.request("GET", endpoint, attributes)
	if err != nil {
		return nil, err
	}

	var mrs []*MergeRequest
	if err = json.Unmarshal(resp.body, &mrs); err != nil {
		return nil, err
	}
	for _, mr := range mrs {
		mr.client = p.client
		mr.endpoint = fmt.Sprintf("%s/%d", endpoint, mr.IID)
	}
	return mrs, nil
}

// MergeRequestByIID returns an individual project merge request
func (p *Project) MergeRequestByIID(mrIID MergeRequestIID, attributes *Attributes) (*MergeRequest, error) {
	var endpoint string
	if p.Links.MergeRequests != nil {
		endpoint = fmt.Sprintf("%s/%d", *p.Links.MergeRequests, mrIID)
	} else {
		endpoint = fmt.Sprintf("%s/projects/%d/merge_requests/%d", p.client.url, p.ID, mrIID)
	}

	resp, err := p.client.request("GET", endpoint, attributes)
	if err != nil {
		return nil, err
	}

	var mr MergeRequest
	if err = json.Unmarshal(resp.body, &mr); err != nil {
		return nil, err
	}
	mr.client = p.client
	mr.endpoint = endpoint

	return &mr, nil
}
