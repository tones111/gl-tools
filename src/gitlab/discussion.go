package gitlab

// DiscussionID indicates the discussion's identifier
type DiscussionID string

// Discussion represents a GitLab discussion
type Discussion struct {
	client   *Client
	endpoint string

	ID             DiscussionID `json:"id"`
	IndividualNote bool         `json:"individual_note"`
	Notes          []*Note      `json:"notes"`
}
