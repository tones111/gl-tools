package gitlab

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

// Client encapsulates the GitLab server API
type Client struct {
	client    *http.Client
	url       string
	apiToken  string
	Verbosity uint8
}

// Attributes contain endpoint variables
type Attributes struct {
	values url.Values
}

// BuildAttributes builds the specified attributes
func BuildAttributes(attrs map[string]string) *Attributes {
	values := url.Values{}
	for k, v := range attrs {
		values.Add(k, v)
	}
	return &Attributes{values: values}
}

// NewClientV4 instantiates an object to facilitate making GitLab api requests
// Note: The url should point to the GitLab server. Example: https://gitlab.com
func NewClientV4(glURL string, apiToken string) (*Client, error) {
	versionURL := glURL + "/api/v4"
	if _, err := url.Parse(versionURL); err != nil {
		return nil, err
	}

	return &Client{
		client:    &http.Client{},
		url:       versionURL,
		apiToken:  apiToken,
		Verbosity: 0,
	}, nil
}

type response struct {
	statusCode int
	header     http.Header
	trailer    http.Header
	body       []byte
}

func (c *Client) request(method, glURL string, attrs *Attributes) (*response, error) {
	req, err := http.NewRequest(method, glURL, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Private-Token", c.apiToken)
	if attrs != nil {
		req.URL.RawQuery = attrs.values.Encode()
	}

	if c.Verbosity > 0 {
		log.Println("Request:", req.URL)
	}

	r, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer safeClose(r.Body, &err)

	resp := response{
		statusCode: r.StatusCode,
		header:     r.Header,
	}

	resp.body, err = ioutil.ReadAll(r.Body)
	if err != nil {
		return &resp, err
	}
	resp.trailer = r.Trailer

	switch c.Verbosity {
	case 0: // TODO: use enum
	case 1:
		log.Println("   Status:", r.Status)
	case 2:
		log.Printf("   Status: %s\n   Response: %s", r.Status, string(prettyJSON(resp.body)))
	}

	return &resp, nil
}

// Projects calls the GitLab /projects endpoint
func (c *Client) Projects(attributes *Attributes) ([]*Project, error) {
	resp, err := c.request("GET", c.url+"/projects", attributes)
	if err != nil {
		return nil, err
	}

	var projects []*Project
	if err = json.Unmarshal(resp.body, &projects); err != nil {
		return nil, err
	}
	for _, proj := range projects {
		proj.client = c
	}
	return projects, nil
}

// ProjectByID calls the GitLab /projects/<id> endpoint
func (c *Client) ProjectByID(id ProjectID, attributes *Attributes) (*Project, error) {
	resp, err := c.request("GET", fmt.Sprintf("%s/projects/%d", c.url, id), attributes)
	if err != nil {
		return nil, err
	}

	var project Project
	if err = json.Unmarshal(resp.body, &project); err != nil {
		return nil, err
	}
	project.client = c

	return &project, nil
}

// MergeRequestByIID retrieves a specific merge request
func (c *Client) MergeRequestByIID(pID ProjectID, mrIID MergeRequestIID, attributes *Attributes) (*MergeRequest, error) {
	endpoint := fmt.Sprintf("%s/projects/%d/merge_requests/%d", c.url, pID, mrIID)
	resp, err := c.request("GET", endpoint, attributes)
	if err != nil {
		return nil, err
	}

	var mr MergeRequest
	if err = json.Unmarshal(resp.body, &mr); err != nil {
		return nil, err
	}
	mr.client = c
	mr.endpoint = endpoint

	return &mr, nil
}

func prettyJSON(raw []byte) []byte {
	var pretty bytes.Buffer
	if json.Indent(&pretty, raw, "", "   ") != nil {
		return raw
	}
	return pretty.Bytes()
}

func safeClose(c io.Closer, err *error) {
	if cerr := c.Close(); cerr != nil && *err == nil {
		*err = cerr
	}
}
