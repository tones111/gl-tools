package gitlab

// These types are used by web hooks or don't have a better place to be yet

// GroupID represents a GitLab group identifier
type GroupID uint64

// MilestoneID indicates the milestone's identifier
type MilestoneID uint64

// PipelineID indicates the pipeline's identifier
type PipelineID uint64

// Commit represents a git commit
type Commit struct {
	ID        string `json:"id"`
	Message   string `json:"message"`
	Timestamp string `json:"timestamp"`
	URL       string `json:"url"`
	Author    struct {
		Email string `json:"email"`
		Name  string `json:"name"`
	} `json:"author"`
}

// Repository represents a GitLab repo
type Repository struct {
	Name        string `json:"name"`
	URL         string `json:"url"`
	Description string `json:"description"`
	Homepage    string `json:"homepage"`
}

// Changes represent a set of changes since the last event
type Changes struct {
	Assignee struct {
		Previous *User `json:"previous"`
		Current  User  `json:"current"`
	} `json:"assignee"`
	Labels struct {
		Previous []Label `json:"previous"`
		Current  []Label `json:"current"`
	} `json:"labels"`
	State struct {
		Previous string `json:"previous"`
		Current  string `json:"current"`
	} `json:"state"`
	TotalTimeSpent struct {
		Previous *uint32 `json:"previous"`
		Current  uint32  `json:"current"`
	} `json:"total_time_spent"`
	UpdatedAt struct {
		Previous string `json:"previous"`
		Current  string `json:"current"`
	} `json:"updated_at"`
}
