package gitlab

// UserID indicates the user's identifier
type UserID uint64

// User represents a gitlab user
type User struct {
	ID        UserID  `json:"id"`
	Name      string  `json:"name"`
	Username  string  `json:"username"`
	State     string  `json:"state"`
	AvatarURL *string `json:"avatar_url"`
	WebURL    *string `json:"web_url"`
}
