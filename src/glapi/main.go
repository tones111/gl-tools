package main

import (
	"gitlab"
	"io/ioutil"
	"log"
	//"strings"
)

func init() {
	log.SetFlags(log.Lshortfile)
}

func main() {
	apiToken, err := ioutil.ReadFile("token")
	if err != nil {
		log.Fatalln("Unable to read API token:", err)
	}

	gl, err := gitlab.NewClientV4("https://gitlab.com", string(apiToken))
	if err != nil {
		log.Fatalln("Unable to create client:", err)
	}
	gl.Verbosity = 1

	gl.Verbosity = 2
	proj, err := gl.ProjectByID(7424178, nil)
	if err != nil {
		log.Fatalln("Unable to get project:", err)
	}

	mr, err := proj.MergeRequestByIID(3, nil)
	if err != nil {
		log.Fatalln("Unable to get mr:", err)
	}

	gl.Verbosity = 2
	//discs, err := mr.Discussions(nil)
	//if err != nil {
	//	log.Fatalln("Unable to get discussions:", err)
	//}
	//for _, disc := range discs {
	//	for _, note := range disc.Notes {
	//		if strings.HasPrefix(note.Body, "ReviewBot:") {
	//			log.Println("ReviewBot Discussion:", disc.ID)
	//			// d9851eae853595f80d30443a340223c8ca3e6e0e

	//			log.Println("ReviewBot Note:", note.ID)
	//			// 88019607
	//		}
	//	}
	//}

	_, err = mr.DiscussionByID("d9851eae853595f80d30443a340223c8ca3e6e0e", nil)
	if err != nil {
		log.Fatalln("Unable to get discussion:", err)
	}

	return

	//projs, err := gl.Projects(gitlab.BuildAttributes(map[string]string{
	//	//"simple":     "true",
	//	"membership": "true",
	//	//"owned":  "true",
	//}))
	//if err != nil {
	//	log.Fatalln("Unable to get projects:", err)
	//}

	// Note: Get individual discussion
	//dID := "eee95997a63c759c6f650675f5d6d7239b7c6666"
	//req, err := http.NewRequest("GET", fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/merge_requests/%s/discussions/%s", id, mrIid, dID), nil)

	// Note: Create new discussion
	// Note: Returns [201] discussion json
	//req, err := http.NewRequest("POST", fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/merge_requests/%s/discussions", id, mrIid), nil)
	//vals.Set("body", "ReviewBot")

	// Note: Modify existing discussion note
	// Note: Returns [200] note json
	//dID := "d9851eae853595f80d30443a340223c8ca3e6e0e"
	//nID := "88019607"
	//req, err := http.NewRequest("PUT", fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/merge_requests/%s/discussions/%s/notes/%s", id, mrIid, dID, nID), nil)
	//vals.Set("body", "ReviewBot: LGTM")

	// Note: Resolve discussion note
	// Note: Returns [200] note json
	//dID := "d9851eae853595f80d30443a340223c8ca3e6e0e"
	//nID := "88019607"
	//req, err := http.NewRequest("PUT", fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/merge_requests/%s/discussions/%s/notes/%s", id, mrIid, dID, nID), nil)
	//vals.Set("resolved", "true")

	//if err != nil {
	//	log.Fatalln("Unable to create request:", err)
	//}
}
