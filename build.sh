#!/usr/bin/env bash

set -e

export GOPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

go build glapi
go build glhook
go build glpipe

# The runner pipeline builds the following:
#make -C gltest
