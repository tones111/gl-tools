package gitlab

// LabelID indicates the label's identifier
type LabelID uint64

// Label represents a GitLab label
type Label struct {
	ID          LabelID   `json:"id"`
	Title       string    `json:"title"`
	Color       string    `json:"color"`
	ProjectID   ProjectID `json:"project_id"`
	CreatedAt   string    `json:"created_at"`
	UpdatedAt   string    `json:"updated_at"`
	Template    bool      `json:"template"`
	Description string    `json:"description"`
	Type        string    `json:"type"`
	GroupID     GroupID   `json:"group_id"`
}
