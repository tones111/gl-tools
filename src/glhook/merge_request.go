package main

import (
	"fmt"
	"gitlab"
	"log"
	"strings"
)

func processMergeRequestHook(mrWH gitlab.MergeRequestHook) error {
	switch mrWH.Attributes.State {
	case "closed", "locked", "merged":
		return nil
	case "opened":
		// Keep going
	}

	targetProj := mrWH.Attributes.Target
	urlLen := strings.Index(targetProj.WebURL, targetProj.PathWithNamespace)
	if urlLen < 0 {
		return fmt.Errorf("Unable to determine server url: (%s, %s)", targetProj.WebURL, targetProj.PathWithNamespace)
	}

	gl, err := gitlab.NewClientV4((targetProj.WebURL)[:urlLen], apiToken)
	if err != nil {
		return err
	}
	gl.Verbosity = 1

	mr, err := gl.MergeRequestByIID(targetProj.ID, mrWH.Attributes.IID, nil)
	if err != nil {
		return err
	}

	log.Printf("ReviewBot scanning MR #%d...\n", mr.ID)

	// Find ReviewBot Discussion / Note
	discs, err := mr.Discussions(nil)
	if err != nil {
		return err
	}
	var rbNote *gitlab.Note
rbSearch:
	for _, disc := range discs {
		for _, note := range disc.Notes {
			if strings.HasPrefix(note.Body, "ReviewBot:") {
				rbNote = note
				break rbSearch
			}
		}
	}

	gl.Verbosity = 2
	if rbNote == nil {
		log.Println("Creating ReviewBot Discussion...")
		if rbDisc, err := mr.NewDiscussion(gitlab.BuildAttributes(map[string]string{
			"body": "ReviewBot: Verifying Commits...",
		})); err != nil {
			return err
		} else if len(rbDisc.Notes) != 1 {
			return fmt.Errorf("New discussion (%s) has a strange number of notes: %d", rbDisc.ID, len(rbDisc.Notes))
		} else {
			rbNote = rbDisc.Notes[0]
		}
	}

	// TODO: validate commits and update rbNote body
	//log.Println("Updating ReviewBot Note...")
	//rbNote.Update(gitlab.BuildAttributes(map[string]string{
	//	"body": "ReviewBot: LGTM",
	//}))
	//}

	return nil
}
