package gitlab

import (
	"encoding/json"
	"fmt"
)

// NoteID indicates the note's identifier
type NoteID uint64

// NoteableID indicates the note's noteable identifier
type NoteableID uint64

// NoteableIID indicates the note's noteable internal identifier
type NoteableIID uint64

// Note represents a GitLab note
type Note struct {
	client   *Client
	endpoint string

	Attachment   *string     `json:"attachment"`
	Author       User        `json:"author"`
	Body         string      `json:"body"`
	CreatedAt    string      `json:"created_at"`
	ID           NoteID      `json:"id"`
	NoteableID   NoteableID  `json:"noteable_id"`
	NoteableIID  NoteableIID `json:"noteable_iid"`
	NoteableType string      `json:"noteable_type"`
	Resolvable   bool        `json:"resolvable"`
	Resolved     bool        `json:"resolved"`
	ResolvedBy   *User       `json:"resolved_by"`
	System       bool        `json:"system"`
	Type         *string     `json:"type"`
	UpdatedAt    string      `json:"updated_at"`
}

// Update modifies an existing note
func (n *Note) Update(attributes *Attributes) (*Note, error) {
	resp, err := n.client.request("POST", n.endpoint, attributes)
	if err != nil {
		return nil, err
	} else if resp.statusCode < 200 || resp.statusCode >= 300 {
		return nil, fmt.Errorf("Unsuccessful: %d", resp.statusCode)
	}

	var note Note
	if err = json.Unmarshal(resp.body, &note); err != nil {
		return nil, err
	}
	note.client = n.client
	note.endpoint = n.endpoint

	return &note, nil
}
