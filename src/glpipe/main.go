package main

import (
	"fmt"
	git "github.com/libgit2/git2go"
	"log"
	"os"
	"strings"
)

func init() {
	log.SetFlags(log.Lshortfile)
}

func main() {
	// Verify GitLab pipeline dependencies are available
	commitRef, refSet := os.LookupEnv("CI_COMMIT_REF_NAME")
	if !refSet {
		log.Fatal("ERROR: Must be run from within a GitLab pipeline")
	}
	commitSha, shaSet := os.LookupEnv("CI_COMMIT_SHA")
	if !shaSet {
		log.Fatal("ERROR: Unable to determine commit hash")
	}

	repo, err := git.OpenRepository(".")
	if err != nil {
		log.Fatal("ERROR:", err)
	}

	var handlerErr error
	for _, meta := range []struct {
		prefix  string
		handler func(*git.Repository, string, string) error
	}{
		{"refs/for/", ReviewUpdate},
		{"refs/merge/", ReviewMerge},
	} {
		target := strings.TrimPrefix(commitRef, meta.prefix)
		if len(target) != len(commitRef) {
			handlerErr = meta.handler(repo, target, commitSha)
			break
		}
	}
	if handlerErr != nil {
		log.Fatal("ERROR: ", handlerErr)
	}
}

// ReviewUpdate todo
func ReviewUpdate(repo *git.Repository, targetBranch, commit string) error {
	log.Print("Processing Review Update...")
	log.Print("target:", targetBranch)
	log.Print("commit:", commit)

	if _, err := repo.References.Lookup("refs/remotes/origin/" + targetBranch); err != nil {
		return fmt.Errorf("%s does not exist upstream", targetBranch)
	}

	return nil
}

// ReviewMerge todo
func ReviewMerge(repo *git.Repository, targetBranch, commit string) error {
	log.Print("Processing Review Merge...")
	log.Print("target:", targetBranch)
	log.Print("commit:", commit)

	if _, err := repo.References.Lookup("refs/remotes/origin/" + targetBranch); err != nil {
		return fmt.Errorf("%s does not exist upstream", targetBranch)
	}

	return nil
}
