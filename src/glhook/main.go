package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"reflect"
)

const (
	port = 8001

	eventMrRequest = "Merge Request Hook"
)

var (
	apiToken  string
	verbosity = 1
)

// validates a webhook request, returning an HTTP status code and error
func validateWebHook(r *http.Request) (int, error) {
	// Verify expected headers are present
	for _, header := range []string{"X-Gitlab-Event", "Content-Type"} {
		if r.Header[header] == nil {
			// Bad Request
			return 400, fmt.Errorf("Missing required header: %s", header)
		}
	}

	// TODO: Verify secret token (X-Gitlab-Token)

	switch r.Method {
	case "POST":
	default:
		// Not Implemented
		return 501, fmt.Errorf("unexpected method (%s) for %v", r.Method, r.Header["X-Gitlab-Event"])
	}

	return 200, nil // OK
}

func parseJSONRequest(data []byte, event string) (int, interface{}, error) {
	var dataI interface{}
	var parseErr error

	switch event {
	case eventMrRequest:
		var mr gitlab.MergeRequestHook
		parseErr = json.Unmarshal(data, &mr)
		dataI = mr

	default:
		// Not Implemented
		return 501, nil, fmt.Errorf("unsupported event: %s", event)
	}

	if parseErr != nil {
		// Internal Server Error
		return 500, nil, fmt.Errorf("json parse error: %v\n%s", parseErr, string(data))
	}

	switch verbosity {
	case 0: // TODO: use enum
	case 1:
		log.Println("EVENT: Merge Request")
	case 2:
		log.Printf("EVENT: Merge Request\n%s", string(prettyJSON(data)))
	}
	return 200, dataI, nil // OK
}

func parseRequest(r *http.Request) (int, interface{}, error) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		// Internal Server Error
		return 500, nil, fmt.Errorf("unable to read body: %v", err)
	}

	// Parse request body based on content type
	switch r.Header["Content-Type"][0] {
	case "application/json":
		return parseJSONRequest(body, r.Header["X-Gitlab-Event"][0])
	}
	// Unsupported Media Type
	return 415, nil, fmt.Errorf("unsupported media type: %v", r.Header["Content-Type"])
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Handle request")
	if status, err := validateWebHook(r); err != nil {
		log.Println("ERROR: request validation:", err)
		w.WriteHeader(status)
		return
	}

	status, dataI, err := parseRequest(r)
	if err != nil {
		log.Println("ERROR: parse:", err)
		w.WriteHeader(status)
		return
	}

	var processErr error
	switch data := dataI.(type) {
	case gitlab.MergeRequestHook:
		processErr = processMergeRequestHook(data)
	default:
		processErr = fmt.Errorf("unexpected data type: %s", reflect.TypeOf(dataI))
	}
	if processErr != nil {
		log.Println("ERROR: processing:", processErr)
		w.WriteHeader(500) // Internal Server Error
		return
	}

	w.WriteHeader(200) // OK
}

func main() {
	// Print these before log.SetOutput so they go to stdout
	if token, err := ioutil.ReadFile("token"); err != nil {
		log.Println("Unable to read API token:", err)
	} else {
		apiToken = string(token)
	}
	fmt.Println("Listening on port", port)

	if logFile, err := os.Create("glhook.log"); err == nil {
		log.SetOutput(logFile)
	}
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)

	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServeTLS(fmt.Sprintf(":%d", port), "cert.pem", "key.pem", nil))
}

func prettyJSON(raw []byte) []byte {
	var pretty bytes.Buffer
	if json.Indent(&pretty, raw, "", "   ") != nil {
		return raw
	}
	return pretty.Bytes()
}
