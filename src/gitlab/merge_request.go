package gitlab

import (
	"encoding/json"
	"fmt"
)

// CommitID indicates the commit's hash
type CommitID string

// MergeRequestID indicates the requests' identifier
type MergeRequestID uint64

// MergeRequestIID indicates the requests' internal identifier
type MergeRequestIID uint64

// MergeRequest represents a GitLab API merge request
type MergeRequest struct {
	client   *Client
	endpoint string

	Assignee *User `json:"assignee"`
	Author   User  `json:"author"`
	//Changes    Changes    `json:"changes"`
	//EventType  string     `json:"event_type"`
	//Labels     []Label    `json:"labels"`
	//ObjectKind string     `json:"object_kind"`
	//Project    Project    `json:"project"`
	//Repository Repository `json:"repository"`
	Squash bool `json:"squash"`
	//User       User       `json:"user"`
	WebURL *string `json:"web_url"`
	//Attributes struct {
	//	Action string `json:"action"`
	//	//Assignee       User           `json:"assignee"`
	//	AssigneeID          UserID         `json:"assignee_id"`
	//	AuthorID            UserID         `json:"author_id"`
	CreatedAt   string `json:"created_at"`
	Description string `json:"description"`
	Downvotes   uint8  `json:"downvotes"`
	//	HeadPipelineID      PipelineID     `json:"head_pipeline_id"`
	//	HumanTimeEstimate   *uint32        `json:"human_time_estimate"`
	//	HumanTotalTimeSpent *uint32        `json:"human_total_time_spent"`
	ID     MergeRequestID  `json:"id"`
	IID    MergeRequestIID `json:"iid"`
	Labels []string        `json:"labels"`
	//	LastCommit          Commit         `json:"last_commit"`
	//	LastEditedAt        *string        `json:"last_edited_at"`
	//	LastEditedByID      *UserID        `json:"last_edited_by_id"`
	MergeCommitSha *CommitID `json:"merge_commit_sha"`
	//	MergeError          *bool          `json:"merge_error"`
	//	MergeParams         struct {
	//		ForceRemoveSourceBranch string `json:"force_remove_source_branch"`
	//	} `json:"merge_params"`
	//	MergeStatus               string       `json:"merge_status"`
	//	MergeUserID               *UserID      `json:"merge_user_id"`
	MergeWhenPipelineSucceeds bool `json:"merge_when_pipeline_succeeds"`
	//	MilestoneID               *MilestoneID `json:"milestone_id"`
	ProjectID ProjectID `json:"project_id"`
	Sha       CommitID  `json:"sha"`
	//	Source                    Project      `json:"source"`
	SourceBranch    string    `json:"source_branch"`
	SourceProjectID ProjectID `json:"source_project_id"`
	State           string    `json:"state"`
	//	Target                    Project      `json:"target"`
	TargetBranch    string    `json:"target_branch"`
	TargetProjectID ProjectID `json:"target_project_id"`
	//	TimeEstimate              uint32       `json:"time_estimate"`
	Title string `json:"title"`
	//	TotalTimeSpent            uint32       `json:"total_time_spent"`
	UpdatedAt string `json:"updated_at"`
	//	UpdatedByID               *UserID      `json:"updated_by_id"`
	Upvotes uint8 `json:"upvotes"`
	//	URL                       string       `json:"url"`
	WorkInProgress bool `json:"work_in_progress"`
	//} `json:"object_attributes"`
}

// MergeRequestHook represents a GitLab webhook merge request event
type MergeRequestHook struct {
	Assignee   User       `json:"assignee"`
	Changes    Changes    `json:"changes"`
	EventType  string     `json:"event_type"`
	Labels     []Label    `json:"labels"`
	ObjectKind string     `json:"object_kind"`
	Project    Project    `json:"project"`
	Repository Repository `json:"repository"`
	User       User       `json:"user"`
	Attributes struct {
		Action string `json:"action"`
		//Assignee       User           `json:"assignee"`
		AssigneeID          *UserID         `json:"assignee_id"`
		AuthorID            UserID          `json:"author_id"`
		CreatedAt           string          `json:"created_at"`
		Description         string          `json:"description"`
		HeadPipelineID      PipelineID      `json:"head_pipeline_id"`
		HumanTimeEstimate   *uint32         `json:"human_time_estimate"`
		HumanTotalTimeSpent *uint32         `json:"human_total_time_spent"`
		ID                  MergeRequestID  `json:"id"`
		IID                 MergeRequestIID `json:"iid"`
		LastCommit          Commit          `json:"last_commit"`
		LastEditedAt        *string         `json:"last_edited_at"`
		LastEditedByID      *UserID         `json:"last_edited_by_id"`
		MergeCommitSha      *string         `json:"merge_commit_sha"`
		MergeError          *bool           `json:"merge_error"`
		MergeParams         struct {
			ForceRemoveSourceBranch string `json:"force_remove_source_branch"`
		} `json:"merge_params"`
		MergeStatus               string       `json:"merge_status"`
		MergeUserID               *UserID      `json:"merge_user_id"`
		MergeWhenPipelineSucceeds bool         `json:"merge_when_pipeline_succeeds"`
		MilestoneID               *MilestoneID `json:"milestone_id"`
		Source                    Project      `json:"source"`
		SourceBranch              string       `json:"source_branch"`
		SourceProjectID           ProjectID    `json:"source_project_id"`
		State                     string       `json:"state"`
		Target                    Project      `json:"target"`
		TargetBranch              string       `json:"target_branch"`
		TargetProjectID           ProjectID    `json:"target_project_id"`
		TimeEstimate              uint32       `json:"time_estimate"`
		Title                     string       `json:"title"`
		TotalTimeSpent            uint32       `json:"total_time_spent"`
		UpdatedAt                 string       `json:"updated_at"`
		UpdatedByID               *UserID      `json:"updated_by_id"`
		URL                       string       `json:"url"`
		WorkInProgress            bool         `json:"work_in_progress"`
	} `json:"object_attributes"`
}

// Discussions returns a list of discussions
func (mr *MergeRequest) Discussions(attributes *Attributes) ([]*Discussion, error) {
	endpoint := mr.endpoint + "/discussions"
	resp, err := mr.client.request("GET", endpoint, attributes)
	if err != nil {
		return nil, err
	}

	var discussions []*Discussion
	if err = json.Unmarshal(resp.body, &discussions); err != nil {
		return nil, err
	}
	for _, discussion := range discussions {
		discussion.client = mr.client
		discussion.endpoint = fmt.Sprintf("%s/%s", endpoint, discussion.ID)
		for _, note := range discussion.Notes {
			note.client = discussion.client
			note.endpoint = fmt.Sprintf("%s/notes/%d", discussion.endpoint, note.ID)
		}
	}
	return discussions, nil
}

// DiscussionByID returns an individual merge request discussion
func (mr *MergeRequest) DiscussionByID(dID DiscussionID, attributes *Attributes) (*Discussion, error) {
	endpoint := fmt.Sprintf("%s/discussions/%s", mr.endpoint, dID)
	resp, err := mr.client.request("GET", endpoint, attributes)
	if err != nil {
		return nil, err
	}

	var discussion Discussion
	if err = json.Unmarshal(resp.body, &discussion); err != nil {
		return nil, err
	}
	discussion.client = mr.client
	discussion.endpoint = endpoint
	for _, note := range discussion.Notes {
		note.client = discussion.client
		note.endpoint = fmt.Sprintf("%s/notes/%d", discussion.endpoint, note.ID)
	}

	return &discussion, nil
}

// NewDiscussion creates a new merge request discussion
func (mr *MergeRequest) NewDiscussion(attributes *Attributes) (*Discussion, error) {
	endpoint := mr.endpoint + "/discussions"
	resp, err := mr.client.request("POST", endpoint, attributes)
	if err != nil {
		return nil, err
	} else if resp.statusCode < 200 || resp.statusCode >= 300 {
		return nil, fmt.Errorf("Unsuccesful: %d", resp.statusCode)
	}

	var discussion Discussion
	if err = json.Unmarshal(resp.body, &discussion); err != nil {
		return nil, err
	}
	discussion.client = mr.client
	discussion.endpoint = fmt.Sprintf("%s/%s", endpoint, discussion.ID)
	for _, note := range discussion.Notes {
		note.client = discussion.client
		note.endpoint = fmt.Sprintf("%s/notes/%d", discussion.endpoint, note.ID)
	}

	return &discussion, nil
}
